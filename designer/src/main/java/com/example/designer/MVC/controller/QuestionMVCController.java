package com.example.designer.MVC.controller;

import com.example.designer.model.Question;
import com.example.designer.service.QuestionService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/main")
public class QuestionMVCController {

    private final QuestionService questionService;

    public QuestionMVCController(QuestionService questionService) {
        this.questionService = questionService;
    }

    @GetMapping("/questions")
    public String showQuestionForm(Model model) {
        model.addAttribute("question", new Question());
        return "questions";
    }

    @PostMapping("/questions")
    public String submitQuestionForm(@ModelAttribute Question question) {
        questionService.saveQuestion(question);
        return "redirect:/questions";
    }
}
