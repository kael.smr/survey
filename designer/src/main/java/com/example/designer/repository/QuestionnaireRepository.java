package com.example.designer.repository;



import com.example.designer.model.Questionnaire;
import org.springframework.stereotype.Repository;

@Repository
public interface QuestionnaireRepository extends GenericRepository<Questionnaire>{
}
