package com.example.designer.repository;

import com.example.designer.model.Question;
import org.springframework.stereotype.Repository;

@Repository
public interface QuestionRepository extends GenericRepository<Question> {

}