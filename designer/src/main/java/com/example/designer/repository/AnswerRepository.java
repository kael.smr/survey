package com.example.designer.repository;



import com.example.designer.model.Answer;
import org.springframework.stereotype.Repository;


@Repository
public interface AnswerRepository extends GenericRepository<Answer> {

}