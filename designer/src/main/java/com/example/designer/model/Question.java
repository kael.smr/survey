package com.example.designer.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Set;


@Setter
@Getter
@NoArgsConstructor
@Entity
@Table(name = "questions")
@SequenceGenerator(name = "default_gen", sequenceName = "authors_seq", allocationSize = 1)
@JsonIdentityInfo(generator = ObjectIdGenerators.UUIDGenerator.class, property = "@json_id")
public class Question extends GenericModel{


    @Column(name = "text")
    private String text;

    @ManyToMany
    @JoinTable(
            name = "answers_question",
            joinColumns = @JoinColumn(name = "question_id"), foreignKey = @ForeignKey(name = "FK_QUESTIONS_ANSWERS"),
            inverseJoinColumns = @JoinColumn(name = "answer_id"), inverseForeignKey = @ForeignKey(name = "FK_ANSWERS_QUESTIONS"))

    private Set<Answer> answerSet;


    @ManyToMany
    @JoinTable(
            name = "questions_questionnaire",
            joinColumns = @JoinColumn(name = "question_id"), foreignKey = @ForeignKey(name = "FK_QUESTIONS_QUESTIONNAIRES"),
            inverseJoinColumns = @JoinColumn(name = "questionnaire_id"), inverseForeignKey = @ForeignKey(name = "FK_QUESTIONNAIRES_QUESTIONS")
    )
    private Set<Questionnaire> questionnaireSet;


}