package com.example.designer.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import jakarta.persistence.*;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Set;


@Setter
@Getter
@NoArgsConstructor
@Entity
@Table(name = "answers")
@SequenceGenerator(name = "default_gen", sequenceName = "authors_seq", allocationSize = 1)
@JsonIdentityInfo(generator = ObjectIdGenerators.UUIDGenerator.class, property = "@json_id")
public class Answer extends GenericModel {

    @Column(name = "text", nullable = false)
    private String text;

    @Column(name = "correct", nullable = false)
    private Boolean correct;

    @ManyToMany
    @JoinTable(
            name = "answers_question",
            joinColumns = @JoinColumn(name = "answer_id"), foreignKey = @ForeignKey(name = "FK_ANSWERS_QUESTIONS"),
            inverseJoinColumns = @JoinColumn(name = "question_id"), inverseForeignKey = @ForeignKey(name = "FK_QUESTIONS_ANSWERS"))
    private Set<Question> questionSet;

}