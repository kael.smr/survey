package com.example.designer.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "questionnaires")
@SequenceGenerator(name = "default_gen", sequenceName = "authors_seq", allocationSize = 1)
@JsonIdentityInfo(generator = ObjectIdGenerators.UUIDGenerator.class, property = "@json_id")
public class Questionnaire extends GenericModel{

    @Column(name = "title", nullable = false)
    private String title;

    @ManyToMany
    @JoinTable(
            name = "questions_questionnaire",
            joinColumns = @JoinColumn(name = "questionnaire_id"), foreignKey = @ForeignKey(name = "FK_QUESTIONNAIRES_QUESTIONS"),
            inverseJoinColumns = @JoinColumn(name = "question_id"), inverseForeignKey = @ForeignKey(name = "FK_QUESTIONS_QUESTIONNAIRES")
    )
    private Set<Question> questionSet;


}
