package com.example.designer.service;

import com.example.designer.dto.QuestionDTO;
import com.example.designer.dto.QuestionWithAnswersDTO;
import com.example.designer.mapper.QuestionMapper;
import com.example.designer.mapper.QuestionWithAnswersMapper;
import com.example.designer.model.Question;
import com.example.designer.repository.QuestionRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class QuestionService extends GenericService<Question, QuestionDTO> {
    private final QuestionRepository questionRepository;
    private final QuestionWithAnswersMapper questionWithAnswersMapper;
    protected QuestionService(QuestionRepository questionRepository,
                              QuestionMapper questionMapper,
                              QuestionWithAnswersMapper questionWithAnswersMapper) {
        super(questionRepository, questionMapper);
        this.questionRepository = questionRepository;
        this.questionWithAnswersMapper = questionWithAnswersMapper;
    }

    public void saveQuestion(Question question) {
        questionRepository.save(question);
    }

    public List<QuestionWithAnswersDTO> getAllQuestionsWithAnswers(){
        return questionWithAnswersMapper.toDTOs(repository.findAll());
    }


}
