package com.example.designer.service;

import com.example.designer.dto.AnswerDTO;
import com.example.designer.mapper.AnswerMapper;
import com.example.designer.model.Answer;
import com.example.designer.repository.AnswerRepository;
import org.springframework.stereotype.Service;

@Service
public class AnswerService extends GenericService<Answer, AnswerDTO> {
    protected AnswerService(AnswerRepository answerRepository, AnswerMapper answerMapper) {
        super(answerRepository, answerMapper);
    }
}
