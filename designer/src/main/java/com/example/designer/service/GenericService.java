package com.example.designer.service;


import com.example.designer.dto.GenericDTO;
import com.example.designer.mapper.GenericMapper;
import com.example.designer.model.GenericModel;
import com.example.designer.repository.GenericRepository;
import org.springframework.stereotype.Service;
import org.webjars.NotFoundException;

import java.util.List;

@Service
public abstract class GenericService<T extends GenericModel, N extends GenericDTO> {
    protected final GenericRepository<T> repository;
    protected final GenericMapper<T,N> mapper;

    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    protected GenericService(GenericRepository<T> repository, GenericMapper<T, N> mapper) {
        this.repository = repository;
        this.mapper = mapper;
    }
    public List<N> listAll(){
        return mapper.toDTOs(repository.findAll());
    }

    public N getOne(final Long id){
        return mapper.toDTO(repository.findById(id).orElseThrow(() -> new NotFoundException(
                "Данных по заданному id: " + id + " не найдено")));
    }

    public void create(N newObject) {
        mapper.toDTO(repository.save(mapper.toEntity(newObject)));
    }

    public N update(N updatedObject) {
        return mapper.toDTO(repository.save(mapper.toEntity(updatedObject)));
    }

    public void delete(final Long id){
        repository.deleteById(id);
    }
}
