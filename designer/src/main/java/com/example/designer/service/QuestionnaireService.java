package com.example.designer.service;

import com.example.designer.dto.QuestionnaireDTO;
import com.example.designer.dto.QuestionnaireWithQuestionsDTO;
import com.example.designer.mapper.QuestionnaireMapper;
import com.example.designer.mapper.QuestionnaireWithQuestionMapper;
import com.example.designer.model.Questionnaire;
import com.example.designer.repository.QuestionnaireRepository;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class QuestionnaireService extends GenericService<Questionnaire, QuestionnaireDTO> {

    private final QuestionnaireWithQuestionMapper questionnaireWithQuestionMapper;

    protected QuestionnaireService(QuestionnaireRepository questionnaireRepository,
                                   QuestionnaireMapper questionnaireMapper,
                                   QuestionnaireWithQuestionMapper questionnaireWithQuestionMapper) {
        super(questionnaireRepository,questionnaireMapper);

        this.questionnaireWithQuestionMapper = questionnaireWithQuestionMapper;
    }
    //TODO: разобраться почему не работает
//    public List<QuestionnaireWithQuestionsDTO> getAllQuestionnaireWithQuestions(){
//        return questionnaireWithQuestionMapper.toDTO(repository.findAll());
//    }

}
