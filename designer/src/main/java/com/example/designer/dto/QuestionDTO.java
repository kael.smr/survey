package com.example.designer.dto;

import lombok.*;

import java.util.Set;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class QuestionDTO extends GenericDTO {
    private String text;
    private Set<Long> answersIds;
    private Set<Long> questionnaireIds;

    public void setAnswersIds(Set<Long> answersIds) {
        this.answersIds = answersIds;
    }
}
