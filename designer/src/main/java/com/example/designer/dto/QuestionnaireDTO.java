package com.example.designer.dto;

import lombok.*;

import java.util.Set;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class QuestionnaireDTO extends GenericDTO {
    private String title;
    private Set<Long> questionsIds;

}
