package com.example.designer.dto;

import lombok.*;

import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class QuestionWithAnswersDTO extends QuestionDTO{
    private Set<AnswerDTO> answers;


}
