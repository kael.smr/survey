package com.example.designer.dto;

import lombok.*;

import java.util.Set;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class AnswerDTO extends GenericDTO{
    private String text;
    private Boolean correct;
    private Set<Long> questionIds;

}
