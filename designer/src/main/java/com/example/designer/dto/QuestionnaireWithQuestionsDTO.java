package com.example.designer.dto;

import lombok.*;

import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class QuestionnaireWithQuestionsDTO extends QuestionnaireDTO{
    private Set<QuestionDTO> questions;
}
