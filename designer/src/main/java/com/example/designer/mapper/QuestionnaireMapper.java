package com.example.designer.mapper;

import com.example.designer.dto.QuestionnaireDTO;
import com.example.designer.model.GenericModel;
import com.example.designer.model.Questionnaire;
import com.example.designer.repository.QuestionRepository;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class QuestionnaireMapper extends GenericMapper<Questionnaire, QuestionnaireDTO> {

    private final QuestionRepository questionRepository;
    protected QuestionnaireMapper(ModelMapper mapper, QuestionRepository questionRepository) {
        super(mapper, Questionnaire.class, QuestionnaireDTO.class);
        this.questionRepository = questionRepository;
    }

    @Override
    protected void mapSpecificFields(QuestionnaireDTO source, Questionnaire destination) {
        if (!Objects.isNull(source.getQuestionsIds())) destination.setQuestionSet(new HashSet<>(questionRepository.findAllById(source.getQuestionsIds())));
        else destination.setQuestionSet(Collections.emptySet());
    }

    @Override
    protected void mapSpecificFields(Questionnaire source, QuestionnaireDTO destination) {
        destination.setQuestionsIds(getIds(source));
    }

    @Override
    protected Set<Long> getIds(Questionnaire entity) {
        return Objects.isNull(entity) || Objects.isNull(entity.getQuestionSet())
                ? null
                : entity.getQuestionSet().stream()
                .map(GenericModel::getId)
                .collect(Collectors.toSet());
    }

    @Override
    protected void setupMapper() {
        modelMapper.createTypeMap(Questionnaire.class,QuestionnaireDTO.class)
                .addMappings(mapping -> mapping.skip(QuestionnaireDTO::setQuestionsIds)).setPostConverter(toDtoConverter());
        modelMapper.createTypeMap(QuestionnaireDTO.class,Questionnaire.class)
                .addMappings(mapping -> mapping.skip(Questionnaire::setQuestionSet)).setPostConverter(toEntityConverter());
    }
}
