package com.example.designer.mapper;

import com.example.designer.dto.QuestionnaireWithQuestionsDTO;
import com.example.designer.model.GenericModel;
import com.example.designer.model.Questionnaire;
import com.example.designer.repository.QuestionRepository;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class QuestionnaireWithQuestionMapper extends GenericMapper<Questionnaire, QuestionnaireWithQuestionsDTO> {
    private final QuestionRepository questionRepository;
    protected QuestionnaireWithQuestionMapper(ModelMapper mapper,
                                              QuestionRepository questionRepository) {
        super(mapper, Questionnaire.class, QuestionnaireWithQuestionsDTO.class);
        this.questionRepository = questionRepository;
    }

    @Override
    protected void mapSpecificFields(QuestionnaireWithQuestionsDTO source, Questionnaire destination) {
        destination.setQuestionSet(new HashSet<>(questionRepository.findAllById(source.getQuestionsIds())));
    }

    @Override
    protected void mapSpecificFields(Questionnaire source, QuestionnaireWithQuestionsDTO destination) {
        destination.setQuestionsIds(getIds(source));
    }

    @Override
    protected Set<Long> getIds(Questionnaire entity) {
        return Objects.isNull(entity) || Objects.isNull(entity.getId())
                ? null
                : entity.getQuestionSet().stream()
                .map(GenericModel::getId)
                .collect(Collectors.toSet());
    }

    @Override
    protected void setupMapper() {
        modelMapper.createTypeMap(Questionnaire.class, QuestionnaireWithQuestionsDTO.class)
                .addMappings(mapping -> mapping.skip(QuestionnaireWithQuestionsDTO::setQuestions)).setPostConverter(toDtoConverter());
        modelMapper.createTypeMap(QuestionnaireWithQuestionsDTO.class,Questionnaire.class)
                .addMappings(mapping -> mapping.skip(Questionnaire::setQuestionSet)).setPostConverter(toEntityConverter());

    }
}
