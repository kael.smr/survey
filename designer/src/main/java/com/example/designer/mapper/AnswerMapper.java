package com.example.designer.mapper;

import com.example.designer.dto.AnswerDTO;
import com.example.designer.model.Answer;
import com.example.designer.model.GenericModel;
import com.example.designer.repository.QuestionRepository;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class AnswerMapper extends GenericMapper<Answer, AnswerDTO> {

    private final QuestionRepository questionRepository;
    protected AnswerMapper(ModelMapper modelMapper,
                           QuestionRepository questionRepository) {
        super(modelMapper, Answer.class, AnswerDTO.class);
        this.questionRepository = questionRepository;

    }

    @Override
    protected void mapSpecificFields(AnswerDTO source, Answer destination) {
        if (!Objects.isNull(source.getQuestionIds())) destination.setQuestionSet(new HashSet<>(questionRepository.findAllById(source.getQuestionIds())));
        else destination.setQuestionSet(Collections.emptySet());
    }

    @Override
    protected void mapSpecificFields(Answer source, AnswerDTO destination) {
        destination.setQuestionIds(getIds(source));
    }

    @Override
    protected Set<Long> getIds(Answer entity) {
        return Objects.isNull(entity) || Objects.isNull(entity.getQuestionSet())
                ? null
                : entity.getQuestionSet().stream()
                .map(GenericModel::getId)
                .collect(Collectors.toSet());
    }

    @Override
    protected void setupMapper() {
        modelMapper.createTypeMap(Answer.class,AnswerDTO.class)
                .addMappings(mapping -> mapping.skip(AnswerDTO::setQuestionIds)).setPostConverter(toDtoConverter());
        modelMapper.createTypeMap(AnswerDTO.class,Answer.class)
                .addMappings(mapping -> mapping.skip(Answer::setQuestionSet)).setPostConverter(toEntityConverter());

    }
}
