package com.example.designer.mapper;

import com.example.designer.dto.QuestionDTO;
import com.example.designer.model.GenericModel;
import com.example.designer.model.Question;

import com.example.designer.repository.AnswerRepository;
import com.example.designer.repository.QuestionnaireRepository;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class QuestionMapper extends GenericMapper<Question, QuestionDTO> {
    private final AnswerRepository answerRepository;
    private final QuestionnaireRepository questionnaireRepository;
    protected QuestionMapper(ModelMapper mapper,
                             AnswerRepository answerRepository,
                             QuestionnaireRepository questionnaireRepository) {
        super(mapper, Question.class, QuestionDTO.class);
        this.answerRepository = answerRepository;
        this.questionnaireRepository = questionnaireRepository;
    }

    @Override
    protected void mapSpecificFields(QuestionDTO source, Question destination) {
        if (!Objects.isNull(source.getAnswersIds())) destination.setAnswerSet(new HashSet<>(answerRepository.findAllById(source.getAnswersIds())));
        else destination.setAnswerSet(Collections.emptySet());

        if (!Objects.isNull(source.getQuestionnaireIds())) destination.setQuestionnaireSet(new HashSet<>(questionnaireRepository.findAllById(source.getQuestionnaireIds())));
        else destination.setQuestionnaireSet(Collections.emptySet());
    }

    @Override
    protected void mapSpecificFields(Question source, QuestionDTO destination) {
        destination.setAnswersIds(getAnswersIds(source));
        destination.setQuestionnaireIds(getQuestionnaireIds(source));
    }

    @Override
    protected Set<Long> getIds(Question entity) {
        return null;
    }

    protected Set<Long> getAnswersIds(Question entity){
        return Objects.isNull(entity) || Objects.isNull(entity.getAnswerSet())
                ? null
                : entity.getAnswerSet().stream()
                .map(GenericModel::getId)
                .collect(Collectors.toSet());
    }
    protected Set<Long> getQuestionnaireIds(Question entity){
        return Objects.isNull(entity) || Objects.isNull(entity.getQuestionnaireSet())
                ? null
                : entity.getQuestionnaireSet().stream()
                .map(GenericModel::getId)
                .collect(Collectors.toSet());
    }

    @Override
    protected void setupMapper() {
        modelMapper.createTypeMap(Question.class,QuestionDTO.class)
                .addMappings(mapping -> mapping.skip(QuestionDTO::setAnswersIds)).setPostConverter(toDtoConverter())
                .addMappings(mapping -> mapping.skip(QuestionDTO::setQuestionnaireIds)).setPostConverter(toDtoConverter());
        modelMapper.createTypeMap(QuestionDTO.class,Question.class)
                .addMappings(mapping -> mapping.skip(Question::setAnswerSet)).setPostConverter(toEntityConverter())
                .addMappings(mapping -> mapping.skip(Question::setQuestionnaireSet)).setPostConverter(toEntityConverter());



    }
}
