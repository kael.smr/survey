package com.example.designer.mapper;

import com.example.designer.dto.GenericDTO;
import com.example.designer.model.GenericModel;

import java.util.List;

public interface Mapper<E extends GenericModel, D extends GenericDTO> {
    E toEntity(D dto);

    List<E> toEntities(List<D> dtos);

    D toDTO(E entity);

    List<D> toDTOs(List<E> entities);
}
