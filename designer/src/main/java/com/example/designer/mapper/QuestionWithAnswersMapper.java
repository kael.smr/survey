package com.example.designer.mapper;

import com.example.designer.dto.QuestionWithAnswersDTO;
import com.example.designer.model.GenericModel;
import com.example.designer.model.Question;
import com.example.designer.repository.AnswerRepository;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class QuestionWithAnswersMapper extends GenericMapper<Question, QuestionWithAnswersDTO>{

    private final AnswerRepository answerRepository;
    protected QuestionWithAnswersMapper(ModelMapper mapper, AnswerRepository answerRepository) {
        super(mapper, Question.class, QuestionWithAnswersDTO.class);
        this.answerRepository = answerRepository;
    }

    @Override
    protected void mapSpecificFields(QuestionWithAnswersDTO source, Question destination) {
        destination.setAnswerSet(new HashSet<>(answerRepository.findAllById(source.getAnswersIds())));
    }

    @Override
    protected void mapSpecificFields(Question source, QuestionWithAnswersDTO destination) {
        destination.setAnswersIds(getIds(source));
    }

    @Override
    protected Set<Long> getIds(Question entity) {
        return Objects.isNull(entity) || Objects.isNull(entity.getId())
                ? null
                : entity.getAnswerSet().stream()
                .map(GenericModel::getId)
                .collect(Collectors.toSet());
    }

    @Override
    protected void setupMapper() {
        modelMapper.createTypeMap(Question.class, QuestionWithAnswersDTO.class)
                .addMappings(mapping -> mapping.skip(QuestionWithAnswersDTO::setAnswersIds)).setPostConverter(toDtoConverter());
        modelMapper.createTypeMap(QuestionWithAnswersDTO.class,Question.class)
                .addMappings(mapping -> mapping.skip(Question::setAnswerSet)).setPostConverter(toEntityConverter());
    }
}
