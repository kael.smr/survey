package com.example.registration.repository;


import com.example.registration.model.User;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends GenericRepository<User>{
    User findUserByLogin(String login);

    User findUserByEmail(String email);


}