package com.example.registration.repository;


import com.example.registration.model.Role;
import org.springframework.stereotype.Repository;





@Repository
public interface RoleRepository extends GenericRepository<Role>{

}