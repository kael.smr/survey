package com.example.registration.mapper;

import com.example.registration.dto.UserDTO;
import com.example.registration.model.User;
import com.example.registration.repository.RoleRepository;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.Set;

@Component
public class UserMapper extends GenericMapper<User, UserDTO> {
    private final ModelMapper modelMapper;
    private final RoleRepository roleRepository;


    protected UserMapper(ModelMapper mapper, ModelMapper modelMapper, RoleRepository roleRepository) {
        super(mapper, User.class, UserDTO.class);
        this.modelMapper = modelMapper;
        this.roleRepository = roleRepository;
    }

    @Override
    protected void mapSpecificFields(UserDTO source, User destination) {
//       destination.setRole(roleRepository.findById(source.getRoleId()).orElseThrow(() -> new NotFoundException("Role not found")));
    }

    @Override
    protected void mapSpecificFields(User source, UserDTO destination) {
//        destination.setRoleId(source.getRole().getId());
    }

    @Override
    protected Set<Long> getIds(User entity) {
        throw new UnsupportedOperationException("Method blocked");
    }

    @Override
    protected void setupMapper() {
//        modelMapper.createTypeMap(User.class,UserDTO.class)
//                .addMappings(m -> m.skip(UserDTO::setRoleId)).setPostConverter(toDtoConverter());
//        modelMapper.createTypeMap(User.class,UserDTO.class)
//                .addMappings(m -> m.skip(UserDTO::setCompletedSurveysIds)).setPostConverter(toDtoConverter());
//        modelMapper.createTypeMap(UserDTO.class,User.class)
//                .addMappings(m -> m.skip(User::setRole)).setPostConverter(toEntityConverter());
//        modelMapper.createTypeMap(UserDTO.class,User.class)
//                .addMappings(m -> m.skip(User::setCompletedSurveySet)).setPostConverter(toEntityConverter());
    }
}
