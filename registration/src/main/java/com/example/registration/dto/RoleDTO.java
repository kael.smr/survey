package com.example.registration.dto;

import lombok.*;

import java.util.Set;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class RoleDTO extends GenericDTO{
    private String title;
    private String description;
    private Set<Long> usersIds;

}
