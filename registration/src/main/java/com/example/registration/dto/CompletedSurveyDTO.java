package com.example.registration.dto;


import lombok.*;

import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class CompletedSurveyDTO extends GenericDTO {
    private Long questionnaireId;
    private Set<Long> usersIds;

}
