package com.example.registration.model;


import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.Set;


@Setter
@Getter
@Entity
@Table(name = "roles")
public class Role extends GenericModel{

    @Column(name = "title")
    private String name;
    @Column(name = "description")
    private String description;
    @OneToMany
    private Set<User> userSet;

}
