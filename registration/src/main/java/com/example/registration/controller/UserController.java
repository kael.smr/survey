package com.example.registration.controller;

import com.example.registration.dto.UserDTO;
import com.example.registration.model.User;
import com.example.registration.service.UserService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/users")
public class UserController extends GenericController<User, UserDTO> {

    protected UserController(UserService userService) {
        super(userService);
    }
}
