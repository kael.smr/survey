package com.example.result.mapper;





import com.example.result.dto.GenericDTO;
import com.example.result.model.GenericModel;

import java.util.List;

public interface Mapper<E extends GenericModel, D extends GenericDTO> {
    E toEntity(D dto);

    List<E> toEntities(List<D> dtos);

    D toDTO(E entity);

    List<D> toDTOs(List<E> entities);
}
