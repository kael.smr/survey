package com.example.result.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.annotation.KafkaListener;

@Configuration
@EnableKafka
public class KafkaConsumerConfig {
    @KafkaListener(topics = "test", groupId = "test-group")
    public void listen(String message) {
        System.out.println("Received message: " + message);
    }
}
