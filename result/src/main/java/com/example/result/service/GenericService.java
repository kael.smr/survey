package com.example.result.service;


import com.example.result.dto.GenericDTO;
import com.example.result.mapper.GenericMapper;
import com.example.result.model.GenericModel;
import com.example.result.repository.GenericRepository;
import org.springframework.stereotype.Service;
import org.webjars.NotFoundException;

import java.util.List;

@Service
public abstract class GenericService<T extends GenericModel, N extends GenericDTO> {
    private final GenericRepository<T> repository;
    private final GenericMapper<T,N> mapper;

    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    protected GenericService(GenericRepository<T> repository, GenericMapper<T, N> mapper) {
        this.repository = repository;
        this.mapper = mapper;
    }
    public List<N> listAll(){
        return mapper.toDTOs(repository.findAll());
    }

    public N getOne(final Long id){
        return mapper.toDTO(repository.findById(id).orElseThrow(() -> new NotFoundException(
                "Данных по заданному id: " + id + " не найдено")));
    }

    public void create(N newObject) {
        mapper.toDTO(repository.save(mapper.toEntity(newObject)));
    }

    public N update(N updatedObject) {
        return mapper.toDTO(repository.save(mapper.toEntity(updatedObject)));
    }

    public void delete(final Long id){
        repository.deleteById(id);
    }
}
