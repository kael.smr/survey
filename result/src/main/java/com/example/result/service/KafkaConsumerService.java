package com.example.result.service;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
public class KafkaConsumerService {
    @KafkaListener(topics = "test", groupId = "test-group")
    public void consume(String message) {
        System.out.println("Received message: " + message);
    }
}

