package com.example.result.model;


import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;



@Getter
@Setter
@Entity
@Table(name = "completed_surveys")
@NoArgsConstructor
@SequenceGenerator(name = "default_gen", sequenceName = "authors_seq", allocationSize = 1)
@JsonIdentityInfo(generator = ObjectIdGenerators.UUIDGenerator.class, property = "@json_id")
public class CompletedSurvey extends GenericModel {

    @Column(name = "title")
    private String title;
}

    //TODO: решить, выводить это в отдельный модуль или нет



