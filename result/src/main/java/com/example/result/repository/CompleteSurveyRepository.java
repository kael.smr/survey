package com.example.result.repository;


import com.example.result.model.CompletedSurvey;
import org.springframework.stereotype.Repository;

@Repository
public interface CompleteSurveyRepository extends GenericRepository<CompletedSurvey> {
}
